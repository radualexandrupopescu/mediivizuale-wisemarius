﻿using System.Windows;
using Tema_2.ViewModels;

namespace Tema_2
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);

            Views.MainWindow mw = new Views.MainWindow();
            MainWindowViewModel mwvm = new MainWindowViewModel();
            mw.DataContext = mwvm;
            mw.Show();
        }
    }
}
