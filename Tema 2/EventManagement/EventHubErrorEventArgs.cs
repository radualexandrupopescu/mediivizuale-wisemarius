﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tema_2.EventManagement
{
    public class EventHubErrorEventArgs : EventArgs
    {
        public EventHubErrorEventArgs(Exception e, Guid token)
        {
            Exception = e;
            GUID = token;
        }

        public Exception Exception { get; }

        public Guid GUID { get; }
    }
}
