﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tema_2.EventManagement
{
    internal sealed class Subscription
    {
        #region Private constants
        //private const long TicksMultiplier = 1000 * TimeSpan.TicksPerMillisecond;
        private const long TicksMultiplier = TimeSpan.TicksPerSecond;
        #endregion

        #region Private variables
        private readonly long _throttleByTicks;
        private double? _lastHandleTimestamp;
        #endregion

        #region Constructors
        internal Subscription(Type type, Guid token, TimeSpan throttleBy, object handler)
        {
            Type = type;
            Token = token;
            Handler = handler;
            _throttleByTicks = throttleBy.Ticks;
        }
        #endregion

        #region Private Properties
        private object Handler { get; }
        #endregion

        #region Internal Properties
        internal Guid Token { get; }
        internal Type Type { get; }
        #endregion

        #region Internal Methods
        internal void Handle<T>(T message)
        {
            if (!CanHandle()) { return; }

            var handler = Handler as Action<T>;
            handler(message);
        }

        internal bool CanHandle()
        {
            if (_throttleByTicks == 0) { return true; }

            if(_lastHandleTimestamp == null)
            {
                _lastHandleTimestamp = Stopwatch.GetTimestamp();
                return true;
            }

            var now = Stopwatch.GetTimestamp();
            var elapsedInTicks = (now - _lastHandleTimestamp) / Stopwatch.Frequency * TicksMultiplier;

            if(elapsedInTicks >= _throttleByTicks)
            {
                _lastHandleTimestamp = now;
                return true;
            }

            return false;
        }
        #endregion
    }
}
