﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Tema_2.EventManagement
{
    public class EventHub : IEventHub
    {
        #region Constructors
        private EventHub() { }
        static EventHub() { }
        #endregion

        #region Singleton implementation
        private static readonly Lazy<EventHub> _instance = new Lazy<EventHub>(() => new EventHub());
        public static EventHub Instance => _instance.Value;
        #endregion

        #region Private members
        private Action<Type, object> _globalHandler;
        private int _disposed;
        #endregion

        #region IEventHub
        public event EventHandler<EventHubErrorEventArgs> OnError;

        public void ClearSubscriptions()
        {
            EnsureNotDisposed();
            Subscriptions.Clear();
        }

        public bool IsSubscribed(Guid token)
        {
            EnsureNotDisposed();
            return Subscriptions.IsRegistered(token);
        }

        public void Publish<T>(T message)
        {
            var localSubscriptions = Subscriptions.GetTheLatestRevisionOfSubscriptions();
            var messageType = typeof(T);

            _globalHandler?.Invoke(messageType, message);

            for (var idx = 0; idx < localSubscriptions.Length; ++idx)
            {
                var subscription = localSubscriptions[idx];

                if (!subscription.Type.IsAssignableFrom(messageType)) { continue; }

                try
                {
                    subscription.Handle(message);
                }
                catch (Exception e)
                {
                    var copy = OnError;
                    copy?.Invoke(this, new EventHubErrorEventArgs(e, subscription.Token));
                }
            }
        }

        public void RegisterGlobalHandler(Action<Type, object> onMessage)
        {
            EnsureNotNull(onMessage);
            EnsureNotDisposed();

            _globalHandler = onMessage;
        }

        public Guid Subscribe<T>(Action<T> action)
        {
            return Subscribe(action, TimeSpan.Zero);
        }

        public Guid Subscribe<T>(Action<T> action, TimeSpan throttleBy)
        {
            EnsureNotNull(action);
            EnsureNotDisposed();

            return Subscriptions.Register(action, throttleBy);
        }

        public void UnSubscribe(Guid token)
        {
            EnsureNotDisposed();

            Subscriptions.UnRegister(token);
        }
        #endregion

        #region Disposal
        public void Dispose()
        {
            Interlocked.Increment(ref _disposed);
            Subscriptions.Dispose();
        }
        #endregion

        #region Helpers
        [DebuggerStepThrough]
        private void EnsureNotDisposed()
        {
            if (_disposed == 1)
            {
                throw new ObjectDisposedException(GetType().Name);
            }
        }

        [DebuggerStepThrough]
        private void EnsureNotNull(object obj)
        {
            if (obj == null)
            {
                throw new NullReferenceException(nameof(obj));
            }
        }
        #endregion
    }
}
