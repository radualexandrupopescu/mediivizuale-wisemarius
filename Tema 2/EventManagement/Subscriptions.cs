﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tema_2.EventManagement
{
    internal static class Subscriptions
    {
        #region Private Variables
        private static readonly List<Subscription> _subscriptions = new List<Subscription>();
        private static int _subscriptionRevision;
        [ThreadStatic]
        private static int _localSubscriptionRevision;
        [ThreadStatic]
        private static Subscription[] _localSubscriptions;
        #endregion

        #region Internal Methods
        internal static Guid Register<T>(Action<T> action, TimeSpan throttleBy)
        {
            var key = Guid.NewGuid();
            var type = typeof(T);
            var subscription = new Subscription(type, key, throttleBy, action);

            lock (_subscriptions)
            {
                _subscriptions.Add(subscription);
                _subscriptionRevision++;
            }

            return key;
        }

        internal static void UnRegister(Guid token)
        {
            var subscription = _subscriptions.FirstOrDefault(s => s.Token == token);
            var removed = _subscriptions.Remove(subscription);

            if (removed) { _subscriptionRevision++; }
        }

        internal static void Clear()
        {
            lock (_subscriptions)
            {
                _subscriptions.Clear();
                _subscriptionRevision++;
            }
        }

        internal static bool IsRegistered(Guid token)
        {
            lock (_subscriptions) { return _subscriptions.Any(s => s.Token == token); }
        }

        internal static Subscription[] GetTheLatestRevisionOfSubscriptions()
        {
            if (_localSubscriptions == null)
            {
                _localSubscriptions = new Subscription[0];
            }

            if (_localSubscriptionRevision == _subscriptionRevision)
            {
                return _localSubscriptions;
            }

            Subscription[] latestSubscriptions;
            lock (_subscriptions)
            {
                latestSubscriptions = _subscriptions.ToArray();
                _localSubscriptionRevision = _subscriptionRevision;
            }
            _localSubscriptions = latestSubscriptions;

            return _localSubscriptions;
        }

        internal static void Dispose()
        {
            Clear();
        }
        #endregion
    }
}
