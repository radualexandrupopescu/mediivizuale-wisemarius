﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tema_2.EventManagement
{
    public interface IEventHub
    {
        event EventHandler<EventHubErrorEventArgs> OnError;

        void RegisterGlobalHandler(Action<Type, object> onMessage);

        void Publish<T>(T message);

        Guid Subscribe<T>(Action<T> action);

        void UnSubscribe(Guid token);

        bool IsSubscribed(Guid token);

        void ClearSubscriptions();
    }
}
