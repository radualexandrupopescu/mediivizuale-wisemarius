﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tema_2.Utils;

namespace Tema_2.Models
{
    [Serializable]
    public class UserModel : ObservableObject, ICloneable
    {
        #region Username property
        private string username;

        public string Username
        {
            get { return username; }
            set { username = value; RaisePropertyChangedEvent("Username"); }
        }
        #endregion

        #region Avatar property
        private string avatar;

        public string Avatar
        {
            get { return avatar; }
            set { avatar = value; RaisePropertyChangedEvent("Avatar"); }
        }
        #endregion

        #region GamesPlayed property
        private short gamesPlayed;

        public short GamesPlayed
        {
            get { return gamesPlayed; }
            set { gamesPlayed = value; RaisePropertyChangedEvent("GamesPlayed"); }
        }
        #endregion

        #region GamesWon property
        private short gamesWon;

        public short GamesWon
        {
            get { return gamesWon; }
            set { gamesWon = value; RaisePropertyChangedEvent("GamesWon"); }
        }
        #endregion

        public object Clone()
        {
            UserModel o = (UserModel)MemberwiseClone();

            return o;
        }
    }
}
