﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tema_2.Utils;
using Tema_2.Models;
using Tema_2.EventManagement;
using System.Windows.Input;
using System.ComponentModel;

namespace Tema_2.ViewModels
{
    class UserViewModel : ObservableObject, IPageViewModel
    {
        #region Private members
        private IEventHub _hub = EventHub.Instance;
        private UserModel _old;

        #endregion

        #region Constructors
        public UserViewModel()
        {
            if (DesignerProperties.GetIsInDesignMode(new System.Windows.DependencyObject()))
                User = new UserModel { Avatar = "C:\\Git\\mediivizuale-wisemarius\\Tema 2\\bin\\Debug\\ProfilePictures\\1.jpg", Username = "DemoUser", GamesPlayed = 1337, GamesWon = 1337 };
            _hub.Subscribe<UserEditEventMessage>(
                e =>
                {
                    // Determine if we are called to edit/create an user
                    // If the check fails we raised this event to signal
                    // we finished editing.
                    if (e.NewValue == null)
                    {
                        // Determine if we were called to edit an user
                        // or to create a new one
                        if (e.OldValue != null)
                        {
                            // Since we have an old value, we must be editing
                            // Do a deep copy so we preserve the original
                            User = (UserModel)e.OldValue.Clone();
                        }
                        else
                        {
                            // We are creating a new user
                            User = new UserModel();
                        }
                        
                        _old = e.OldValue;

                        User.PropertyChanged += User_PropertyChanged;
                    }
                }
            );

            okCommand = new DelegateCommand<object>(
                execM =>
                {
                    User.PropertyChanged -= User_PropertyChanged;
                    _hub.Publish(new UserEditEventMessage { OldValue = _old, NewValue = User });
                },
                canExecM =>
                {
                    return !string.IsNullOrEmpty(User?.Username) &&
                        !string.IsNullOrWhiteSpace(User?.Username) &&
                        !string.IsNullOrEmpty(User?.Avatar);
                    //return true;
                }
            );
            nextAvatarCommand = new DelegateCommand<object>(
                execM =>
                {
                    int idx = 0;
                    if (User.Avatar != null)
                    {
                        idx = Array.FindIndex(ProfilePictures.Element, p => p == User.Avatar);
                        ++idx;
                        if (idx >= ProfilePictures.Element.Length)
                            idx = idx - ProfilePictures.Element.Length;
                    }
                    User.Avatar = ProfilePictures.Element[idx];
                }
            );
            prevAvatarCommand = new DelegateCommand<object>(
                execM =>
                {
                    int idx = 0;
                    if (User.Avatar != null)
                    {
                        idx = Array.FindIndex(ProfilePictures.Element, p => p == User.Avatar);
                        --idx;
                        if (idx < 0)
                            idx = ProfilePictures.Element.Length - idx;
                    }
                    User.Avatar = ProfilePictures.Element[idx];
                }
            );
        }
        #endregion

        #region User Property
        private UserModel user;

        public UserModel User
        {
            get { return user; }
            set { user = value; RaisePropertyChangedEvent("User"); }
        }
        #endregion

        #region OkCommand Property
        private DelegateCommand<object> okCommand;

        public DelegateCommand<object> OKCommand
        {
            get { return okCommand; }
        }

        private void User_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            OKCommand.RaiseCanExecuteChanged();
        }
        #endregion

        #region NextAvatarCommand Property
        private DelegateCommand<object> nextAvatarCommand;

        public DelegateCommand<object> NextAvatarCommand
        {
            get { return nextAvatarCommand; }
        }
        #endregion

        #region PrevAvatarCommand Property
        private DelegateCommand<object> prevAvatarCommand;

        public DelegateCommand<object> PrevAvatarCommand
        {
            get { return prevAvatarCommand; }
        }
        #endregion

        #region IPageViewModel Properties
        public string Name
        {
            get { return "User"; }
        }
        #endregion
    }
}
