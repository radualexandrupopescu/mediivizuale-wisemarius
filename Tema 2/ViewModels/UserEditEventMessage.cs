﻿using Tema_2.Models;

namespace Tema_2.ViewModels
{
    internal class UserEditEventMessage
    {
        public UserModel OldValue;
        public UserModel NewValue;
    }
}