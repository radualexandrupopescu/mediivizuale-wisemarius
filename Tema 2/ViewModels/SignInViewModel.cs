﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tema_2.Utils;
using Tema_2.Models;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows.Input;
using Tema_2.EventManagement;

namespace Tema_2.ViewModels
{
    public class SignInViewModel : ObservableObject, IPageViewModel
    {
        #region Private Members
        private IEventHub _hub = EventHub.Instance;
        #endregion

        #region Constructors
        public SignInViewModel()
        {
#if DEBUG   // stupid overkill optimization
            if (DesignerProperties.GetIsInDesignMode(new System.Windows.DependencyObject()))
            {
                Random r = new Random();
                if (_users == null)
                    _users = new ObservableCollection<UserModel>();
                for (int i = 0; i < ProfilePictures.Element.Length; i++)
                {
                    short games = (short)r.Next();
                    short gamesWon = (short)(games * r.NextDouble());
                    //_users.Add(new User(string.Format("DesignUser {0}", i), ProfilePictures.Element[i], games, gamesWon
                    _users.Add(new UserModel {
                        Username = string.Format("DesignUser {0}", i),
                        Avatar = "C:\\Git\\mediivizuale-wisemarius\\Tema 2\\bin\\Debug\\ProfilePictures\\" + i.ToString() + ".jpg",
                        GamesPlayed = games,
                        GamesWon = gamesWon
                    });
                }
                //SelectedUser = _users[r.Next() % _users.Count];
                SelectedUser = _users[5];
            }
            else
            {
#endif
                var u = SerializationManagerSingleton.Instance.Deserialize<UserModel>("demousers.bin") as IEnumerable<UserModel>;
                if (u != null)
                {
                    _users = new ObservableCollection<UserModel>(u);
                }
                else
                {
                    _users = new ObservableCollection<UserModel>();
                }
#if DEBUG
            }
#endif
            _editUserCommand = new DelegateCommand<object>(
                execM =>
                {
                    _hub.Publish(new UserEditEventMessage { OldValue = SelectedUser });
                },
                canExecM =>
                {
                    return SelectedUser != null;
                }
            );
            _createUserCommand = new DelegateCommand<object>(
                execM =>
                {
                    _hub.Publish(new UserEditEventMessage { });
                }
            );

            _hub.Subscribe<UserEditEventMessage>(e =>
            {
                if (e.OldValue != null && e.NewValue != null)
                {
                    int idx = Array.FindIndex(_users.ToArray(), o => o == e.OldValue);
                    _users[idx] = e.NewValue;
                    SelectedUser = e.NewValue;
                }
                if (e.OldValue == null && e.NewValue != null)
                {
                    _users.Add(e.NewValue);
                    SelectedUser = e.NewValue;
                }
            });
        }
        #endregion

        #region Users Property
        private ObservableCollection<UserModel> _users;
        public ObservableCollection<UserModel> Users
        {
            get
            {
                if (_users == null)
                    _users = new ObservableCollection<UserModel>();
                return _users;
            }
        }
        #endregion

        #region SelectedUser Property
        private UserModel _selectedUser;

        public UserModel SelectedUser
        {
            get { return _selectedUser; }
            set { _selectedUser = value; RaisePropertyChangedEvent("SelectedUser"); }
        }

        #endregion

        #region Edit User Command Property
        private DelegateCommand<object> _editUserCommand;

        public DelegateCommand<object> EditUserCommand
        {
            get { return _editUserCommand; }
        }
        #endregion

        #region Create User Command Property
        private DelegateCommand<object> _createUserCommand;

        public DelegateCommand<object> CreateUserCommand
        {
            get { return _createUserCommand; }
            set { _createUserCommand = value; }
        }
        #endregion

        #region IPageViewModel Properties
        public string Name
        {
            get { return "SignIn"; }
        }
        #endregion
    }
}
