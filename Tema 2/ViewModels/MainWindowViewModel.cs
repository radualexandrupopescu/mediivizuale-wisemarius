﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tema_2.EventManagement;
using Tema_2.Models;
using Tema_2.Utils;

namespace Tema_2.ViewModels
{
    public class MainWindowViewModel: ObservableObject
    {
        #region Private Members
        private EventHub _hub = EventHub.Instance;
        #endregion

        #region Constructors
        public MainWindowViewModel()
        {
            if (_viewModels == null)
                _viewModels = new List<IPageViewModel>();
            _viewModels.Add(new SignInViewModel());
            _viewModels.Add(new UserViewModel());

            _currentViewModel = _viewModels[0];

            _hub.Subscribe<UserEditEventMessage>(
                e =>
                {
                    if (e.OldValue != null && e.NewValue != null)
                    {
                        // Returned from editing an user
                        CurrentViewModel = _viewModels[0];
                    }
                    if (e.OldValue == null && e.NewValue != null)
                    {
                        // Returned from creating a new user
                        CurrentViewModel = _viewModels[0];
                    }
                    if(e.NewValue == null)
                    {
                        // Entering creating or editing an user
                        CurrentViewModel = _viewModels[1];
                    }
                }
            );
        }
        #endregion

        #region ViewModels Property
        private List<IPageViewModel> _viewModels;
        public List<IPageViewModel> ViewModels
        {
            get
            {
                if (_viewModels == null)
                    _viewModels = new List<IPageViewModel>();
                return _viewModels;
            }

        }
        #endregion

        #region CurrentViewModel Property
        private IPageViewModel _currentViewModel;
        public IPageViewModel CurrentViewModel
        {
            get
            {
                return _currentViewModel;
            }
            set
            {
                if(value != _currentViewModel)
                {
                    _currentViewModel = value;
                    RaisePropertyChangedEvent("CurrentViewModel");
                }
            }
        }
        #endregion

        #region UserChangeHandler
        void NewUserCreatedHandler(Models.UserModel user)
        {
            CurrentViewModel = _viewModels[0];
        }
        #endregion
    }
}
