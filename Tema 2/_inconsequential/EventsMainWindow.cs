﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace Tema_2
{
	partial class MainWindow : Window
	{
		private void newUserButton_Click(object sender, RoutedEventArgs e)
		{
			var newUser = new NewUser(usersManager);
			newUser.ShowDialog();
		}

		private void cancelButton_Click(object sender, RoutedEventArgs e)
		{
			this.Close();
		}

		private void deleteUserButton_Click(object sender, RoutedEventArgs e)
		{
			int indexToRemove = usersListBox.SelectedIndex;
			usersManager.deleteUser(usersManager.ElementAt(indexToRemove).name);
		}

		private void mainWindow_closing(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (MessageBox.Show("Close Application ?", "Question", MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.Yes)
			{
				usersManager.Serialize();
			}
			else
			{
				e.Cancel = true;
			}
		}

		private void usersListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			deleteUserButton.IsEnabled = true;
			playButton.IsEnabled = true;
		}

		private void previousButton_Click(object sender, RoutedEventArgs e)
		{

		}

		private void nextButton_Click(object sender, RoutedEventArgs e)
		{

		}
	}
}
