﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Tema_2
{
	public partial class NewUser : Window
	{
		private void buttonSubmit_Click(object sender, RoutedEventArgs e)
		{
			if(textBoxUsername.Text == "" || usersManager.userExists(textBoxUsername.Text))
			{
				MessageBox.Show("Please chose a valid username");
			}
			else
			{
				User user = new User(textBoxUsername.Text);
				usersManager.Add(user);

				this.Close();
			}
		}
	}
}
