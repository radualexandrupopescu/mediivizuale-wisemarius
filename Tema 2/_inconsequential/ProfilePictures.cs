﻿using System;

namespace Tema_2
{
	class ProfilePictures
	{
		public static string[] Element { get; set; } = 
		{
			AppDomain.CurrentDomain.BaseDirectory + @"ProfilePictures\1.jpg",
			AppDomain.CurrentDomain.BaseDirectory + @"ProfilePictures\2.png",
			AppDomain.CurrentDomain.BaseDirectory + @"ProfilePictures\3.jpg",
			AppDomain.CurrentDomain.BaseDirectory + @"ProfilePictures\4.jpg",
			AppDomain.CurrentDomain.BaseDirectory + @"ProfilePictures\5.jpg",
			AppDomain.CurrentDomain.BaseDirectory + @"ProfilePictures\6.jpg",
			AppDomain.CurrentDomain.BaseDirectory + @"ProfilePictures\7.jpg"
		};

		public static int index = 0;
	}
}
