﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace Tema_2
{
	[Serializable]
	public class UsersManager : ObservableCollection<User>
	{
		public bool userExists(string username)
		{
			foreach (User user in this)
			{
				if (user.name == username)
				{
					return true;
				}
			}

			return false;
		}

		public void deleteUser(string username)
		{
			Remove(Items.Where(d => d.name == username).ElementAt(0));
		}

		public void Serialize()
		{
			SerializationManager.Serialize(this);
		}

		public void Deserialize()
		{
			UsersManager serializedUsers = SerializationManager.Deserialize();

			Clear();

			if (serializedUsers != null)
			{
				foreach (var user in serializedUsers)
				{
					Add(user);
				}
			}
		}
	}
}
