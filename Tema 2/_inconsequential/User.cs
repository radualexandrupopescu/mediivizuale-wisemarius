﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tema_2
{
	[Serializable]
	public class User
	{
		public User(string name, string avatar, short games, short gamesWon)
		{
			this.name = name;
			this.avatar = avatar;
			this.games = games;
			this.gamesWon = gamesWon;
		}

		public User(string name)
		{
			this.name = name;
			this.avatar = null;
			this.games = 0;
			this.gamesWon = 0;
		}

		public string name { get; set; }
		public string avatar { get; set; } 
		public short games { get; set; }
		public short gamesWon { get; set; }

		public override string ToString()
		{
			return this.name;
		}
	}
}
