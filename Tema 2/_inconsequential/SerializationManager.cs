﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace Tema_2
{
	class SerializationManager
	{
		public static void Serialize(UsersManager usersManager)
		{
			IFormatter formatter = new BinaryFormatter();
			Stream stream = new FileStream("users.bin", FileMode.Create, FileAccess.Write, FileShare.None);
			formatter.Serialize(stream, usersManager);
			stream.Close();
		}

		public static UsersManager Deserialize()
		{
			IFormatter formatter = new BinaryFormatter();
			UsersManager obj = null;

			if (File.Exists("users.bin"))
			{
				Stream stream = new FileStream("users.bin", FileMode.Open, FileAccess.Read, FileShare.Read);
				obj = (UsersManager)formatter.Deserialize(stream);
				stream.Close();
			}

			return obj;
		}
	}
}
