﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Tema_2
{
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window
	{
		UsersManager usersManager = new UsersManager();
		string link { get; set; } = ProfilePictures.Element.ElementAt(0);

		public MainWindow()
		{
			InitializeComponent();

			usersManager.Deserialize();

			Resources["Users"] = usersManager;
		}
	}
}
