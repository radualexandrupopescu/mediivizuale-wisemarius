﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Tema_2.Utils
{
    public class DelegateCommand<T> : DelegateCommandBase
    {
        #region Private members
        private readonly Action<T> _executeMethod;
        private readonly Func<T, bool> _canExecuteMethod;
        #endregion

        #region Constructors
        public DelegateCommand(Action<T> executeMethod, Func<T, bool> canExecuteMethod) : base()
        {
            if (executeMethod == null || canExecuteMethod == null)
                throw new ArgumentNullException(nameof(executeMethod), "DelegateCommand arguments can not be null.");

            TypeInfo genericTypeInfo = typeof(T).GetTypeInfo();
            if (genericTypeInfo.IsValueType)
                if ((!genericTypeInfo.IsGenericType) || (!typeof(Nullable<>).GetTypeInfo().IsAssignableFrom(genericTypeInfo.GetGenericTypeDefinition().GetTypeInfo())))
                    throw new InvalidCastException("T for DelegateCommand<T> is not an object nor Nullable.");

            _executeMethod = executeMethod;
            _canExecuteMethod = canExecuteMethod;
        }

        public DelegateCommand(Action<T> executeMethod) : this(executeMethod, (o) => true) { }
        #endregion

        #region ICommand implemented methods
        public bool CanExecute(T parameter)
        {
            return _canExecuteMethod(parameter);
        }

        public void Execute(T parameter)
        {
            _executeMethod(parameter);
        }
        #endregion

        #region DelegateCommandBase overriden members
        protected override bool CanExecute(object parameter)
        {
            return CanExecute((T)parameter);
        }

        protected override void Execute(object parameter)
        {
            Execute((T)parameter);
        }
        #endregion
    }
}
