﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Tema_2.Utils
{
    public abstract class DelegateCommandBase : ICommand
    {
        #region Private members
        private SynchronizationContext _synchronizationContext;

        private readonly HashSet<string> _propertiesToObserve = new HashSet<string>();
        private INotifyPropertyChanged _inpc;
        #endregion

        #region Constructors
        public DelegateCommandBase()
        {
            _synchronizationContext = SynchronizationContext.Current;
        }
        #endregion

        #region ICommand CanExecuteChanged handler
        protected virtual void OnCanExecuteChanged()
        {
            var handler = CanExecuteChanged;
            if (handler != null)
            {
                if (_synchronizationContext != null && _synchronizationContext != SynchronizationContext.Current)
                {
                    _synchronizationContext.Post((o) => handler.Invoke(o, EventArgs.Empty), null);
                }
                else
                {
                    handler.Invoke(this, EventArgs.Empty);
                }
            }
        }

        public void RaiseCanExecuteChanged()
        {
            OnCanExecuteChanged();
        }
        #endregion

        #region ICommand members
        public virtual event EventHandler CanExecuteChanged;

        bool ICommand.CanExecute(object parameter)
        {
            return CanExecute(parameter);
        }

        void ICommand.Execute(object parameter)
        {
            Execute(parameter);
        }
        #endregion

        #region ICommand helpers
        protected abstract bool CanExecute(object parameter);

        protected abstract void Execute(object parameter);
        #endregion
    }
}
