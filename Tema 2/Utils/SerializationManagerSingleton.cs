﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using Tema_2.Models;

namespace Tema_2.Utils
{
    public sealed class SerializationManagerSingleton
    {
        #region Singleton Implementation
        private static readonly Lazy<SerializationManagerSingleton> _instance = new Lazy<SerializationManagerSingleton>(() => new SerializationManagerSingleton());

        private SerializationManagerSingleton() { }

        public static SerializationManagerSingleton Instance { get { return _instance.Value; } }
        #endregion

        #region Serialization and Deserialization
        public void Serialize<T>(IEnumerable<T> obj, string fileName, IFormatter formatter)
        {
            using (var fs = new FileStream(fileName, FileMode.Create, FileAccess.Write, FileShare.Read))
            {
                formatter.Serialize(fs, obj);
            }
        }

        public void Serialize<T>(IEnumerable<T> obj, string fileName)
        {
            IFormatter formatter = new BinaryFormatter();
            Serialize(obj, fileName, formatter);
        }

        public IEnumerable<T> Deserialize<T>(string fileName, IFormatter formatter)
        {
            IEnumerable<T> ret = null;
            try
            {
                using (var fs = new FileStream(fileName, FileMode.Open, FileAccess.Read, FileShare.Read))
                {
                    ret = (IEnumerable<T>)formatter.Deserialize(fs);
                }
            }
            catch (FileNotFoundException e)
            {
                // pass;
#if DEBUG
                Debug.WriteLine(e);
#endif
            }
            catch (Exception e)
            {
                // pass;
#if DEBUG
                Debug.WriteLine(e);
#endif
            }
            return ret;
        }

        public IEnumerable<T> Deserialize<T>(string fileName)
        {
            IFormatter formatter = new BinaryFormatter();
            return Deserialize<T>(fileName, formatter);
        }
        #endregion
    }
}
