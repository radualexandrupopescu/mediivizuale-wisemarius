﻿namespace Tema_2.Utils
{
    public interface IPageViewModel
    {
        string Name { get; }
    }
}
